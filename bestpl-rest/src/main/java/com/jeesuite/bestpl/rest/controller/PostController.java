/**
 * 
 */
package com.jeesuite.bestpl.rest.controller;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jeesuite.bestpl.api.IPostService;
import com.jeesuite.bestpl.dto.Comment;
import com.jeesuite.bestpl.dto.IdNamePair;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年10月25日
 */
@Component
@Singleton
@Path("/post")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
public class PostController {
	
	@Autowired
	private IPostService service;
	
	@GET
	@Path("categorys")
	public List<IdNamePair> findAllPostCategory(){
		return service.findAllPostCategory();
	}
	
	@POST
	@Path("add")
	public boolean addPost(Post post){
		service.addPosts(post);
		return true;
	}
	
	
	@GET
	@Path("{id}")
	public Post getById(@PathParam("id") int id){
		return service.findPostById(id);
	}
	
	@GET
	@Path("{sortType}/{topLimit}")
	public List<Post> findTopPost(@QueryParam("categoryId") int categoryId,@PathParam("sortType") String sortType,@PathParam("topLimit") int topLimit){
		return service.findTopPost(categoryId, sortType, topLimit);
	}
	
	@POST
	@Path("list")
	public Page<Post> pageQueryPost(final PageQueryParam param){
		return service.pageQueryPost(param);
	}
	
	@POST
	@Path("add_comment")
	public Boolean addPostComment(Comment comment){
		service.addPostComment(comment);
		return true;
	}
	
	@POST
	@Path("comment/list")
	public Page<Comment> pageQueryPostComment(@QueryParam("postId") int postId,@QueryParam("pageNo") int pageNo,@QueryParam("pageSize") int pageSize){
		return service.pageQueryPostComment(postId, pageNo, pageSize);
	}

}
