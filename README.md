### 项目介绍
1. 这是一个轻社区web应用，作为使用jeesuite开发分布式系统的实践项目。
2. 麻雀虽小(业务简单)，五脏俱全(常用分布式场景都有体现)。
3. 为了演示一些分布式场景，请忽略某些业务拆分的合理性。
4. 仅为了练手，没有提供后台功能（毕竟后台系统开源的太多了）

### 支持
 - 交流群：230192763 
 - 基于[jeesuite-libs](http://git.oschina.net/vakinge/jeesuite-libs) 
 - jeesuite统一管理平台[jeesuite-admin](http://git.oschina.net/vakinge/jeesuite-admin) 
 

### 依赖说明（如果出现包找不到或者某些class没找到）
- SNAPSHOT结尾的依赖包请自行下载[jeesuite-libs](http://git.oschina.net/vakinge/jeesuite-libs) 项目本地构建。
- release版的依赖请配置最高的版本号，版本号请看：[sonatype](https://oss.sonatype.org/content/repositories/releases/com/jeesuite/) 

### 模块说明
- bestpl-api 定义接口
- bestpl-dao 数据访问接口
- bestpl-service 服务实现
- bestpl-dubbo 基于dubbo服务发布
- bestpl-rest 基于jersey restAPI服务发布
- bestpl-springboot 基于springboot应用服务

### 涉及场景
 - 分布式session (`完成`)
 - Oauth2.0第三方登录集成 (`完成`)
 - restAPI自定义协议包装  (`完成`)
 - 分布式缓存  (`完成`)
 - 一级/二级缓存同步  (`完成`)
 - 自动crud  (`完成`)
 - 读写分离  (`完成`)
 - db操作自动缓存  (`完成`)
 - 分布式定时任务  (`完成`)
 - 可靠消息队列(跨系统异步交互) (`完成`)
 - 分布式锁 (`完成`)
 - 全局ID生成器 (`完成`)
 - event Bus  (`完成`)
 - dubbo集成  (`完成`)
 - springboot集成 (`完成`)
 - docker构建  (`完成`)
 - 配置中心  (`完成`)
 - swagger API文档输出 (`完成`)
  - 全文检索  (`未完待续..`)
 - API网关(服务路由、流量控制,授权检查、安全检查)  (`未完待续..`)
 - 应用监控  (`未完待续..`)
 - 熔断机制  (`未完待续..`)
 - 链路跟踪  (`未完待续..`)
 - ELK日志方案  (`未完待续..`)

### 架构图
![image](http://ojmezn0eq.bkt.clouddn.com/bestpl_arch.png)
### 业务交互图
![image](http://ojmezn0eq.bkt.clouddn.com/bestpl_sequence.png)


### 如何发布服务？
项目已经配置了阿里云的基础环境（配置中心、数据库、redis，kafka等），下载即可运行。


**特别说明：**
 1. 如果使用已经配置好的基础环境，分别修改每个模块配置项`jeesuite.configcenter.profile`为`dev`，如果需要本地搭建全套环境请加群：`230192763`拿资料
 2. 应用启动可以不启动rest模块。rest模块作为提供restful api独立模块，适用于前后端分离场景。
 
#### 基于docker发布
```
#切换到项目目录
cd /你的项目目录/jeesuite-bestpl
#打包，发布镜像到docker（下载镜像可能耗时很久）
mvn clean install -DskipTests=true -Pdocker
#启动
docker-compose up -d

```

#### eclipse调试运行
  1. **dubbo**：运行`AppServer.java`
  2. **rest**:通过`mvn jetty:run`运行
  3. **springboot**：运行`Application.java` 
  
#### 构建手动发布
- 构建：mvn clean install -DskipTests=true
- 启动
  1. **dubbo**：解压构建`bestpl-dubbo-dist.zip`，运行`appserver.sh`或`appserver.bat`
  2. **rest**:直接通过`tomcat`发布
  3. **springboot**：`java -jar bestpl-springboot.jar` 


### 预览截图
![image](http://ojmezn0eq.bkt.clouddn.com/bestpl_snapshot.png)






