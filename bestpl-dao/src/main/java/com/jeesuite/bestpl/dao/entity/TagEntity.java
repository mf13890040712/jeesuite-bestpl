package com.jeesuite.bestpl.dao.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jeesuite.mybatis.core.BaseEntity;

@Table(name = "sc_tags")
public class TagEntity extends BaseEntity {
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 标签类别(1:普通标签，2:代码标签(类名、函数等))
     */
    @Column(name = "tag_type")
    private Integer tagType;

    /**
     * 标签名称
     */
    private String name;

    /**
     * 引用次数
     */
    @Column(name = "refer_count")
    private Integer referCount;

    /**
     * 获取主键ID
     *
     * @return id - 主键ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取标签类别(1:普通标签，2:代码标签(类名、函数等))
     *
     * @return tag_type - 标签类别(1:普通标签，2:代码标签(类名、函数等))
     */
    public Integer getTagType() {
        return tagType;
    }

    /**
     * 设置标签类别(1:普通标签，2:代码标签(类名、函数等))
     *
     * @param tagType 标签类别(1:普通标签，2:代码标签(类名、函数等))
     */
    public void setTagType(Integer tagType) {
        this.tagType = tagType;
    }

    /**
     * 获取标签名称
     *
     * @return name - 标签名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置标签名称
     *
     * @param name 标签名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取引用次数
     *
     * @return refer_count - 引用次数
     */
    public Integer getReferCount() {
        return referCount;
    }

    /**
     * 设置引用次数
     *
     * @param referCount 引用次数
     */
    public void setReferCount(Integer referCount) {
        this.referCount = referCount;
    }
}