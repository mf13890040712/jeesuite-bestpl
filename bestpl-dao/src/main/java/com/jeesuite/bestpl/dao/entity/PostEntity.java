package com.jeesuite.bestpl.dao.entity;

import com.jeesuite.mybatis.core.BaseEntity;
import java.util.Date;
import javax.persistence.*;

@Table(name = "sc_posts")
public class PostEntity extends BaseEntity {
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 语言id
     */
    @Column(name = "user_id")
    private Integer userId;

    /**
     * 语言id
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 语言名称
     */
    private String title;

    /**
     * 标签多个,隔开
     */
    private String tags;

    /**
     * 栏目id
     */
    @Column(name = "category_id")
    private Integer categoryId;

    /**
     * 评论数
     */
    @Column(name = "comment_count")
    private Integer commentCount;

    /**
     * 评论数
     */
    @Column(name = "view_count")
    private Integer viewCount;

    /**
     * 是否推荐
     */
    @Column(name = "is_recommend")
    private Boolean isRecommend;

    /**
     * 是否原创
     */
    @Column(name = "is_original")
    private Boolean isOriginal;

    /**
     * 创建时间
     */
    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "seo_keywords")
    private String seoKeywords;

    @Column(name = "seo_desc")
    private String seoDesc;

    /**
     * 摘要
     */
    private String summary;

    /**
     * 内容
     */
    private String content;

    /**
     * 获取主键ID
     *
     * @return id - 主键ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取语言id
     *
     * @return user_id - 语言id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 设置语言id
     *
     * @param userId 语言id
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 获取语言id
     *
     * @return user_name - 语言id
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置语言id
     *
     * @param userName 语言id
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取语言名称
     *
     * @return title - 语言名称
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置语言名称
     *
     * @param title 语言名称
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取标签多个,隔开
     *
     * @return tags - 标签多个,隔开
     */
    public String getTags() {
        return tags;
    }

    /**
     * 设置标签多个,隔开
     *
     * @param tags 标签多个,隔开
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * 获取栏目id
     *
     * @return category_id - 栏目id
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * 设置栏目id
     *
     * @param categoryId 栏目id
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * 获取评论数
     *
     * @return comment_count - 评论数
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * 设置评论数
     *
     * @param commentCount 评论数
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * 获取评论数
     *
     * @return view_count - 评论数
     */
    public Integer getViewCount() {
        return viewCount;
    }

    /**
     * 设置评论数
     *
     * @param viewCount 评论数
     */
    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    /**
     * 获取是否推荐
     *
     * @return is_recommend - 是否推荐
     */
    public Boolean getIsRecommend() {
        return isRecommend;
    }

    /**
     * 设置是否推荐
     *
     * @param isRecommend 是否推荐
     */
    public void setIsRecommend(Boolean isRecommend) {
        this.isRecommend = isRecommend;
    }

    /**
     * 获取是否原创
     *
     * @return is_original - 是否原创
     */
    public Boolean getIsOriginal() {
        return isOriginal;
    }

    /**
     * 设置是否原创
     *
     * @param isOriginal 是否原创
     */
    public void setIsOriginal(Boolean isOriginal) {
        this.isOriginal = isOriginal;
    }

    /**
     * 获取创建时间
     *
     * @return created_at - 创建时间
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建时间
     *
     * @param createdAt 创建时间
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return seo_keywords
     */
    public String getSeoKeywords() {
        return seoKeywords;
    }

    /**
     * @param seoKeywords
     */
    public void setSeoKeywords(String seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    /**
     * @return seo_desc
     */
    public String getSeoDesc() {
        return seoDesc;
    }

    /**
     * @param seoDesc
     */
    public void setSeoDesc(String seoDesc) {
        this.seoDesc = seoDesc;
    }

    /**
     * 获取摘要
     *
     * @return summary - 摘要
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 设置摘要
     *
     * @param summary 摘要
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 获取内容
     *
     * @return content - 内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    public void setContent(String content) {
        this.content = content;
    }
}