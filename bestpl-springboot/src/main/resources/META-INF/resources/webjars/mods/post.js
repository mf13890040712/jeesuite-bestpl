layui.define(['laypage', 'fly','form','code'], function(exports){

  var $ = layui.jquery;
  var layer = layui.layer;
  var util = layui.util;
  var laytpl = layui.laytpl;
  var form = layui.form;
  var laypage = layui.laypage;
  var fly = layui.fly;
  
  var gather = {}, dom = {
    comment: $('#comment')
    ,content: $('#L_content')
    ,commentCount: $('#commentCount')
  };

  //提交回答
  fly.form['/jie/reply/'] = function(data, required){
    var tpl = '<li>\
      <div class="detail-about detail-about-reply">\
        <a class="jie-user" href="/user/">\
          <img src="{{= d.user.avatar}}" alt="{{= d.user.username}}">\
          <cite>{{d.user.username}}</cite>\
        </a>\
        <div class="detail-hits">\
          <span>刚刚</span>\
        </div>\
      </div>\
      <div class="detail-body comment-body">\
        {{ d.content}}\
      </div>\
    </li>'
    data.content = fly.content(data.content);
    laytpl(tpl).render($.extend(data, {
      user: layui.cache.user
    }), function(html){
      required[0].value = '';
      dom.comment.find('.fly-none').remove();
      dom.comment.append(html);
      
      var count = dom.commentCount.text()|0;
      dom.commentCount.html(++count);
    });
  };

  //评论操作
  gather.commentActive = {
    reply: function(li){ //回复
      var val = dom.content.val();
      var aite = '@'+ li.find('.jie-user cite i').text().replace(/\s/g, '');
      dom.content.focus()
      if(val.indexOf(aite) !== -1) return;
      dom.content.val(aite +' ' + val);
    }
  };
  $('.comment-reply span').on('click', function(){
    var othis = $(this), type = othis.attr('type');
    gather.commentActive[type].call(this, othis.parents('li'));
  });

  exports('post', null);
});