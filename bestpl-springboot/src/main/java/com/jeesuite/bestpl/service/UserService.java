package com.jeesuite.bestpl.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesuite.bestpl.dao.entity.OpenidBindEntity;
import com.jeesuite.bestpl.dao.entity.UserEntity;
import com.jeesuite.bestpl.dao.entity.UserEntity.UserStatus;
import com.jeesuite.bestpl.dao.mapper.OpenidBindEntityMapper;
import com.jeesuite.bestpl.dao.mapper.UserEntityMapper;
import com.jeesuite.bestpl.exception.JeesuiteBaseException;
import com.jeesuite.bestpl.security.PasswordUtils;

@Service
public class UserService {

	@Autowired
	private UserEntityMapper userMapper;
	
	@Autowired
	private OpenidBindEntityMapper openidBindMapper;

	
	@Transactional
	public UserEntity createUser(UserEntity userEntity,String snsType,String openId){
		
		if(StringUtils.isBlank(userEntity.getUsername()))throw new JeesuiteBaseException("用户名不能为空");
		if(StringUtils.isBlank(userEntity.getEmail()))throw new JeesuiteBaseException("邮箱不能为空");
		if(StringUtils.isBlank(userEntity.getPassword()))throw new JeesuiteBaseException("密码不能为空");
		
		if( userMapper.findByName(userEntity.getUsername()) != null){
			throw new JeesuiteBaseException("用户名已存在");
		}
		
		if( userMapper.findByEmail(userEntity.getEmail()) != null){
			throw new JeesuiteBaseException("邮箱已存在");
		}
		
		userEntity.setPassword(PasswordUtils.encrypt(userEntity.getPassword(), userEntity.getUsername()));
		userEntity.setStatus(UserStatus.a.name());
		userEntity.setUserType(1);
		userEntity.setCreatedAt(new Date());
		userEntity.setLoggedAt(userEntity.getCreatedAt());
		userMapper.insertSelective(userEntity);
		
		return userEntity;
	}
	
	public void updateUser(UserEntity user) {
		UserEntity entity = userMapper.selectByPrimaryKey(user.getId());
		//未认证可以修改
		if (StringUtils.isNotBlank(user.getEmail()) && !entity.getEmailVerifyed()) {
			UserEntity emailUser = userMapper.findByEmail(user.getEmail());
			if(emailUser != null && !emailUser.getId().equals(entity.getId())){
				throw new JeesuiteBaseException("邮箱已存在");
			}
			entity.setRealname(user.getEmail());
		}
		
		if (StringUtils.isNotBlank(user.getMobile()) && !entity.getMobileVerifyed()) {
			UserEntity emailUser = userMapper.findByMobile(user.getMobile());
			if(emailUser != null && !emailUser.getId().equals(entity.getId())){
				throw new JeesuiteBaseException("手机号已存在");
			}
			entity.setRealname(user.getEmail());
		}
		
		if (StringUtils.isNotBlank(user.getNickname())) {
			entity.setNickname(user.getNickname());
		}
		if (StringUtils.isNotBlank(user.getRealname())) {
			entity.setRealname(user.getRealname());
		}
		if (StringUtils.isNotBlank(user.getAvatar())) {
			entity.setAvatar(user.getAvatar());
		}
		if (StringUtils.isNotBlank(user.getSignature())) {
			entity.setSignature(user.getSignature());
		}
		if (StringUtils.isNotBlank(user.getPassword())) {
			entity.setPassword(PasswordUtils.encrypt(user.getPassword(), entity.getUsername()));
		}
		userMapper.updateByPrimaryKeySelective(entity);
	}
	
	public UserEntity findUserbyOpenIdAndType(String type,String openId){
		OpenidBindEntity bindEntity = openidBindMapper.findByOpenIdAndType(openId, type);
		if(bindEntity == null )return null;
		return userMapper.selectByPrimaryKey(bindEntity.getUserId());
	}
	
	public UserEntity findUserbyLoginName(String loginName){
		//TODO 手机、邮箱
		return userMapper.findByName(loginName);
	}
}
