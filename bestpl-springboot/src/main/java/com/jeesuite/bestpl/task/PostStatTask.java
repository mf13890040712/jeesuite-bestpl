package com.jeesuite.bestpl.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jeesuite.bestpl.service.PostApiService;
import com.jeesuite.scheduler.AbstractJob;
import com.jeesuite.scheduler.JobContext;
import com.jeesuite.scheduler.annotation.ScheduleConf;


@Service
@ScheduleConf(cronExpr="0 */5 * * * ?",jobName="postStatTask",executeOnStarted = true)
public class PostStatTask extends AbstractJob {

	@Autowired
	private PostApiService postApiService;
	
	@Override
	public boolean parallelEnabled() {
		return false;
	}

	@Override
	public void doJob(JobContext context) throws Exception {
		
	}

}
