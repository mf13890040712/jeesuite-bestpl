package com.jeesuite.bestpl.event;

import com.google.common.eventbus.Subscribe;
import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.common.json.JsonUtils;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;

public class MessageSendEventListener {

	private TopicProducerSpringProvider topicProducerProvider; 
	private Message lastMessage;
	
	
	
	public MessageSendEventListener(TopicProducerSpringProvider topicProducerProvider) {
		this.topicProducerProvider = topicProducerProvider;
	}

	@Subscribe  
    public void listen(Message message) {  
        lastMessage = message;  
        //通过kafka发送
        topicProducerProvider.publishNoWrapperMessage("_msg_topic", JsonUtils.toJson(message));
    }

	public Message getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(Message lastMessage) {
		this.lastMessage = lastMessage;
	}  
	
	
}
