package com.jeesuite.bestpl.security;

import com.jeesuite.common.util.DigestUtils;

public class PasswordUtils {

	private static final String GLOBAL_SALT = "2#$5~!eded*(f4";
	public static String encrypt(String password,String salt){
		return DigestUtils.md5WithSalt(password, GLOBAL_SALT + salt);
	}
}
