package com.jeesuite.bestpl.security;

import java.io.Serializable;

public class LoginUserInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String avatar;
	private String level;
	//用户类型(1:普通用户,2:内部编辑)
	private int userType;
	
	
	public LoginUserInfo(int id, String name, int userType) {
		super();
		this.id = id;
		this.name = name;
		this.userType = userType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	
	
}
